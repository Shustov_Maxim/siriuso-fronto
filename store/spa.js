
export default {
  namespaced: true,
  state() {
    return {
      spa_params: {},
      spa_component: null,
      spa_fetch: null,
      spa_data: null,
    }
  },
  getters: {
    params(state) {
      return state.spa_params;
    },
    component(state) {
      return state.spa_component;
    },
    fetch(state) {
      return state.spa_fetch;
    },
    data(state) {
      return state.spa_data;
    }
  },
  mutations: {
    setSPAParams(state, data) {
      if(typeof data === 'object') {
        state.spa_params = data
      }
    },
    setSPAComp(state, data) {
        state.spa_component = data
    },
    setSPAFetch(state, data) {
      state.spa_fetch = data
    },
    setSPAData(state, data) {
      state.spa_data = data
    }
  }
}
